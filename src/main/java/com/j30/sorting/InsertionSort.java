package com.j30.sorting;

public class InsertionSort {
    public static void sort(int[] tablica) {

        int licznik = 0;

        // zaczynamy od dwóch elementów.
        for (int i = 1; i < tablica.length; i++) {
            int elementAktualniePorownywany = tablica[i];

            int indeksPorownywany = i - 1;
            while (indeksPorownywany >= 0) {
                licznik++;
                if (elementAktualniePorownywany < tablica[indeksPorownywany]) {
                    // swap
                    int tmp = tablica[indeksPorownywany];
                    tablica[indeksPorownywany] = tablica[indeksPorownywany + 1];
                    tablica[indeksPorownywany + 1] = tmp;
                } else {
                    break;
                }

                indeksPorownywany--;
            }
//            tablica[indeksPorownywany + 1] = elementAktualniePorownywany;
        }
        System.out.println("Licznik instrukcji: " + licznik);
    }
}
