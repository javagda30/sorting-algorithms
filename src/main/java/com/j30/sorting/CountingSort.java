package com.j30.sorting;

public class CountingSort {
    public static void sort(int tablica[]) {
        // nasze sortowanie zakłada tylko wartości dodatnie!
        // musimy odnaleźć minimum i maksimum

        int licznik = 0;
        int max = tablica[0];
        for (int i = 1; i < tablica.length; i++) {
            licznik++;
            if (max < tablica[i]) {
                max = tablica[i];
            }
        }
        System.out.println("Maksimum: " + max);

        int[] tablicaZliczen = new int[max + 1];

        for (int i = 0; i < tablica.length; i++) {
            licznik++;
            tablicaZliczen[tablica[i]]++;
        }

        int indeksWstawiania = 0;
        for (int j = 0; j < tablicaZliczen.length; j++) { // idz przez wszystkie wartości
//            for (int i = 0; i < tablicaZliczen[j]; i++) {
            while ((tablicaZliczen[j]--) > 0) {
                licznik++;
                tablica[indeksWstawiania++] = j;
            }
        }

        System.out.println("Licznik operacji: 30");
        System.out.println("Licznik operacji: " + licznik);
    }
}
