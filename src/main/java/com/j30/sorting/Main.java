package com.j30.sorting;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int[] tablicaPes = new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        int[] tablicaOpt = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] tablicaRand = new int[]{6, 3, 7, 1, 5, 4, 10, 8, 9, 2};

        System.out.println("Pesymistycznie:");
//        BubbleSort.sort(tablicaPes);
//        InsertionSort.sort(tablicaPes);
        CountingSort.sort(tablicaPes);
        System.out.println(Arrays.toString(tablicaPes));

        System.out.println("Optymistycznie:");
//        BubbleSort.sort(tablicaOpt);
//        InsertionSort.sort(tablicaOpt);
        CountingSort.sort(tablicaOpt);
        System.out.println(Arrays.toString(tablicaOpt));

        System.out.println("Pawło-losowo:");
//        BubbleSort.sort(tablicaRand);
//        InsertionSort.sort(tablicaRand);
        CountingSort.sort(tablicaRand);
        System.out.println(Arrays.toString(tablicaRand));

    }
}
